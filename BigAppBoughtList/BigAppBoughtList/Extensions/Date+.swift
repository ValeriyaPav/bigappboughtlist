//
//  Date+.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 20.09.21.
//

import Foundation

extension Date {
    
    var startOfMonth: Date {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)
        return calendar.date(from: components)!
    }
    
    var endOfMonth: Date {
           var components = DateComponents()
           components.month = 1
           components.second = -1
           return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth)!
       }
    
    static func differenceInMonthesBetweenTwoDates(firstDate: Date, lastDate: Date) -> [Date] {
        if firstDate > lastDate { return [Date]() }
        var tempDate = firstDate
        var array = [tempDate]
        while tempDate < lastDate {
            tempDate = Calendar.current.date(byAdding: .month, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        return array
    }
    
}


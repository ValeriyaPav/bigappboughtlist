//
//  Fonts+.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 25.09.21.
//

import Foundation
import UIKit

extension UIFont {
    
    static func robotoRegularFont(size: Int) -> UIFont {
        return customFontWith(name: "Roboto-Regular", size: size)
    }
    
    static func oswaldRegularFont(size: Int) -> UIFont {
        return customFontWith(name: "Oswald-Regular", size: size)
    }
    
    
    static func pangolinRegularFont(size: Int) -> UIFont {
        return customFontWith(name: "Pangolin-Regular", size: size)
    }
    
    private static func customFontWith(name: String, size: Int) -> UIFont {
        return UIFont(name: name, size: CGFloat(size))!
    }
    
}

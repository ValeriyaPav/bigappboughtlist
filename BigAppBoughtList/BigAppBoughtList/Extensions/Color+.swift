//
//  Color+.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 20.09.21.
//

import Foundation

import UIKit

extension UIColor{
    
    static var myBaseColor: UIColor {
        let baseColor = UIColor(red: 46.0/255.0, green: 137.0/255.0, blue: 221.0/255.0, alpha: 1.0)
        return baseColor
    }
    
    static var myButtonColor: UIColor {
        let buttonColor = UIColor(red: 209.0/255.0, green: 129.0/255.0, blue: 8.0/255.0, alpha: 1.0)
        return buttonColor
    }
    
    static var myBigTextColor: UIColor {
        let bigTextColor = UIColor(red: 209.0/255.0, green: 129.0/255.0, blue: 8.0/255.0, alpha: 1.0)
        return bigTextColor
    }
    
    static var appYellowColor: UIColor {
        let yellowColor = UIColor(red: 255.0/255.0, green: 213.0/255.0, blue: 65.0/255.0, alpha: 1.0)
        return yellowColor
    }
    
    static var appRedColor: UIColor {
        let redColor = UIColor(red: 214.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        return redColor
    }
    
    static var appBlueColor: UIColor {
        let blueColor = UIColor(red: 96.0/255.0, green: 224.0/255.0, blue: 201.0/255.0, alpha: 1.0)
        return blueColor
    }
    
    static var appPurpleColor: UIColor {
        
        let purpleColor = UIColor(red: 154.0/255.0, green: 17.0/255.0, blue: 166.0/255.0, alpha: 1.0)
        return purpleColor
    }
    
    static var appDarkBlueColor: UIColor {
        
        let darkBlueColor = UIColor(red: 4.0/255.0, green: 7.0/255.0, blue: 90.0/255.0, alpha: 1.0)
        return darkBlueColor
    }
    
    static var appOrangeColor: UIColor {
        
        let orangeColor = UIColor(red: 217.0/255.0, green: 111.0/255.0, blue: 14.0/255.0, alpha: 1.0)
        return orangeColor
    }
    
    static var appGreenColor: UIColor {
        
        let greenColor = UIColor(red: 23.0/255.0, green: 198.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        return greenColor
    }
}



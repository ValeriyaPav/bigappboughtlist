//
//  UserStorageManager.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import Foundation
import UIKit

class UserStorageManager {
    
    static private let dataKey: String = "kBoughtArray"
    static private let colorKey: String = "kAppColor"
    static private let fontKey: String = "kAppFont"
    
    private static func saveBoughtArray(array: [BoughtModel]) {
        var dataArray: [Data] = []
        for element in array {
            if let data = element.asData {
                dataArray.append(data)
            }
        }
        UserDefaults.standard.setValue(dataArray, forKey: dataKey)
    }
    
    static func getBoughtArray() -> [BoughtModel] {
        if let dataArray: [Data] = UserDefaults.standard.value(forKey: dataKey) as? [Data] {
            var boughtArray: [BoughtModel] = []
            for data in dataArray {
                let decoder: JSONDecoder = JSONDecoder()
                do {
                    let model = try decoder.decode(BoughtModel.self, from: data)
                    boughtArray.append(model)
                } catch {
                    debugPrint(error.localizedDescription)
                }
            }
            return boughtArray
        }
        return []
    }
    
    static func addBoughtModel(model: BoughtModel) {
        var currentDBArray = getBoughtArray()
        currentDBArray.append(model)
        saveBoughtArray(array: currentDBArray)
    }
    
    static func removeBoughtModel(id: String) {
        var currentDBArray = getBoughtArray()
        currentDBArray.removeAll(where: { element in
            element.id == id
        })
        saveBoughtArray(array: currentDBArray)
    }
    
    static func saveColor(color: AppColors){
        let colorString: String = color.rawValue
        UserDefaults.standard.set(colorString, forKey: colorKey)
    }
    
    static func getColor() -> UIColor{
        var color: UIColor = UIColor.myBaseColor
        if let colorString: String = UserDefaults.standard.value(forKey: colorKey) as? String {
            if let appColors: AppColors = AppColors(rawValue: colorString) {
                color = appColors.colors
            }
        }
        return color
    }
    
    static func saveFont(font: AppFonts){
        let fontString: String = font.rawValue
        UserDefaults.standard.set(fontString, forKey: fontKey)
    }
    
    static func getFont(size: Int) -> UIFont {
        var font: UIFont = UIFont.systemFont(ofSize: CGFloat(size))
        if let fontString: String = UserDefaults.standard.value(forKey: fontKey) as? String{
            if let appFont: AppFonts = AppFonts(rawValue: fontString){
                font = appFont.fontWithSize(size: size)
            }
        }
        return font
    }
    
}

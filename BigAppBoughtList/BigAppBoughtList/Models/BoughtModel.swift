//
//  BoughtModel.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import Foundation
import UIKit

struct BoughtModel: Codable {
    
    let id: String
    let price: Double
    let categoryImageName: String
    let category: Category
    let dateBought: Double
    let description: String
    let photoBought: Data?
    
    var asData: Data? {
        let encoder: JSONEncoder = JSONEncoder()
        if let data: Data = try? encoder.encode(self) {
            return data
        }
        return nil
    }
}

//
//  AppFonts.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 25.09.21.
//

import Foundation
import UIKit

enum AppFonts: String, CaseIterable, Codable {
    case pangolin = "Pangolin-Regular"
    case roboto = "Roboto-Regular"
    case oswald = "Oswald-Regular"
    case system
    
    func fontWithSize(size: Int) -> UIFont {
        switch self {
        case .pangolin:
            return UIFont.pangolinRegularFont(size: size)
        case .roboto:
            return UIFont.robotoRegularFont(size: size)
        case .oswald:
            return UIFont.oswaldRegularFont(size: size)
        case .system:
            return UIFont.systemFont(ofSize: CGFloat(size))
        }
    }
}

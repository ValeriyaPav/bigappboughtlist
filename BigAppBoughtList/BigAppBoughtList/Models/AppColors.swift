//
//  AppColors.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 24.09.21.
//

import Foundation
import UIKit

enum AppColors: String, CaseIterable, Codable{
    
    case yellowColor
    case redColor
    case blueColor
    case purpleColor
    case darkBlueColor
    case orangeColor
    case greenColor
    
    var colors: UIColor {
        switch self {
        
        case .yellowColor:
            return UIColor.appYellowColor
        case .redColor:
            return UIColor.appRedColor
        case .blueColor:
            return UIColor.appBlueColor
        case .purpleColor:
            return UIColor.appPurpleColor
        case .darkBlueColor:
            return UIColor.appDarkBlueColor
        case .orangeColor:
            return UIColor.appOrangeColor
        case .greenColor:
            return UIColor.appGreenColor
        }
    }
    
}

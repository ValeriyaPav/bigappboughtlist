//
//  Category.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 25.09.21.
//

import Foundation
import UIKit

enum Category: Int, CaseIterable, Codable {
    case food
    case restaurants
    case payments
    case travel
    case plays
    case entertainment
    case clothes
    case householdChemicals
    case appliances
    case medicine
    case other
    
    var categoryName: String {
        switch self {
        case .food:
            return "Продукты"
        case .restaurants:
            return "Рестораны"
        case .payments:
            return "Платежи"
        case .travel:
            return "Проезд"
        case .plays:
            return "Игры"
        case .entertainment:
            return "Развлечения"
        case .clothes:
            return "Одежда"
        case .householdChemicals:
            return "Быт - Хим"
        case .appliances:
            return "Техника"
        case .medicine:
            return "Медицина"
        case .other:
            return "Прочее"
        }
    }
    
    var categoryImageName: String {
        switch self {
        case .food:
            return "Food"
        case .restaurants:
            return "restaurants"
        case .payments:
            return "payments"
        case .travel:
            return "taxi"
        case .plays:
            return "play"
        case .entertainment:
            return "entertainment"
        case .clothes:
            return "clothes"
        case .householdChemicals:
            return "household chemicals"
        case .appliances:
            return "Appliances"
        case .medicine:
            return "medicine"
        case .other:
            return "other"
        }
    }
}

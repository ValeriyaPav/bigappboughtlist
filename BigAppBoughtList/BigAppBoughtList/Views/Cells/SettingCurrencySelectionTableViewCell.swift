//
//  SettingCurrencySelectionTableViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 9.09.21.
//

import UIKit

class SettingCurrencySelectionTableViewCell: UITableViewCell {
    
    static let reuseId: String = "SettingCurrencySelectionTableViewCell"
    static let cellHeight: CGFloat = 52.0
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var currencySelecterButton: UIButton!
    @IBOutlet weak var nameCurrencyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let image: UIImage? = UIImage(named: "SelecterNoSelect")?.withRenderingMode(.alwaysOriginal)
        self.currencySelecterButton.setImage(image, for: UIControl.State.normal)
        
       
        //self.currencySelecterButton.tintColor = .black
        
        self.separatorView.backgroundColor = UserStorageManager.getColor()
    }

    @IBAction func currencySelecterActionButton(_ sender: Any) {
        let image1: UIImage? = UIImage(named: "SelecterSelect")?.withRenderingMode(.alwaysOriginal)
        self.currencySelecterButton.setImage(image1, for: UIControl.State.normal)
    }
    
}

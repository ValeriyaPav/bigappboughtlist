//
//  MonthListTableViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit

class MonthListTableViewCell: UITableViewCell {
    
    static let reuseId: String = "MonthListTableViewCell"
    static let cellHeight: CGFloat = 80.0
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var allSpentSumLabel: UILabel!
    @IBOutlet weak var allSpentLabel: UILabel!
    @IBOutlet weak var nameMonthLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.allSpentLabel.text = "Всего потрачено:"
        self.separatorView.backgroundColor = UserStorageManager.getColor()
        self.nameMonthLabel.textColor = UserStorageManager.getColor()
    }

    
}

//
//  CategoriesCollectionViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 8.09.21.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellLayerView: UIView!
    @IBOutlet weak var categoriesImageView: UIImageView!
    @IBOutlet weak var nameCategoriesLabel: UILabel!
    static let reuseId: String = "CategoriesCollectionViewCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cellLayerView.layer.cornerRadius = 18.0
        self.cellLayerView.layer.borderWidth = 2.0
        self.cellLayerView.layer.borderColor = UserStorageManager.getColor().cgColor
    }

}

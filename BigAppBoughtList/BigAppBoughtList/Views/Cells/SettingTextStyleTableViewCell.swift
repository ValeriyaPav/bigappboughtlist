//
//  SettingTextStyleTableViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 9.09.21.
//

import UIKit

class SettingTextStyleTableViewCell: UITableViewCell {
    
    static let reuseId: String = "SettingTextStyleTableViewCell"
    static let cellHeight: CGFloat = 52.0
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var textStyleSelectButton: UIButton!
    @IBOutlet weak var textStyleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.textStyleSelectButton.setImage(UIImage(named: "SelecterNoSelect"), for: UIControl.State.normal)
        self.textStyleSelectButton.tintColor = .black
        
        self.textStyleSelectButton.setImage(UIImage(named: "SelecterSelect"), for: UIControl.State.selected)
        
        self.separatorView.backgroundColor = UserStorageManager.getColor()
    }

  
    @IBAction func textStyleSelectActionButton(_ sender: Any) {
    }
    
}

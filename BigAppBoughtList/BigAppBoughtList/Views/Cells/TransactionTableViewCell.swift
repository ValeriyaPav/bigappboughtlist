//
//  TransactionTableViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    
    static let reuseId: String = "TransactionTableViewCell"
    static let cellHeight: CGFloat = 80.0
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var priceBoughtLabel: UILabel!
    @IBOutlet weak var categoryBoughtLabel: UILabel!
    @IBOutlet weak var desсriptionBoughtLabel: UILabel!
    @IBOutlet weak var dateBoughtLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.separatorView.backgroundColor = UserStorageManager.getColor()
        self.desсriptionBoughtLabel.textColor = UserStorageManager.getColor()
    }
    
    func configureWithModel(model: BoughtModel) {
        self.priceBoughtLabel.text = String(model.price)
        let date: Date = Date(timeIntervalSince1970: model.dateBought)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        self.dateBoughtLabel.text = formatter.string(from: date)
        self.categoryImageView.image = UIImage(named: model.categoryImageName)
        self.categoryBoughtLabel.text = String(model.category.categoryName)
        self.desсriptionBoughtLabel.text = model.description
    }
}

//
//  DarkThemeTableViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 8.09.21.
//

import UIKit

class DarkThemeTableViewCell: UITableViewCell {
    
    @IBAction func switchTapped(_ sender: Any) {
        if darkThemeSwitch.isOn {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .dark
            }
        } else {
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .light
            }
        }
    }
    
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var nameDarkThemeSettingLabel: UILabel!
    @IBOutlet weak var darkThemeSwitch: UISwitch!
    
    static let reuseId: String = "DarkThemeTableViewCell"
    static let cellHeight: CGFloat = 52.0

    override func awakeFromNib() {
        super.awakeFromNib()
        self.separatorView.backgroundColor = UserStorageManager.getColor()
    }

    
}

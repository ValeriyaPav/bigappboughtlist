//
//  SettingsTableViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    static let reuseId: String = "SettingsTableViewCell"
    static let cellHeight: CGFloat = 52.0
    
    @IBOutlet weak var separatorView: UIView!
    var monthTextColor = UserStorageManager.getColor()
    
    @IBOutlet weak var openSettingButton: UIButton!
    @IBOutlet weak var nameSettingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.openSettingButton.setImage(UIImage(named: "open"), for: UIControl.State.normal)
        self.openSettingButton.tintColor = .black
        self.separatorView.backgroundColor = UserStorageManager.getColor()
        
    }

}

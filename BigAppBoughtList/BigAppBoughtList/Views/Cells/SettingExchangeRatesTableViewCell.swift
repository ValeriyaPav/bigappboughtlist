//
//  ExchangeRatesTableViewCell.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 9.09.21.
//

import UIKit

class SettingExchangeRatesTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    static let reuseId: String = "SettingExchangeRatesTableViewCell"
    static let cellHeight: CGFloat = 52.0
    
    @IBOutlet weak var separatorVerticatView: UIView!
    @IBOutlet weak var exchangeTextField: UITextField!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var exchangeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.exchangeTextField.delegate = self
        self.separatorView.backgroundColor = UserStorageManager.getColor()
        self.separatorVerticatView.backgroundColor = UserStorageManager.getColor()
        
    }
    
}

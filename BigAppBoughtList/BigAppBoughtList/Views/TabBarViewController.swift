//
//  TabBarViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit

class TabBarViewController: UITabBarController {

    @IBOutlet weak var boughtListTabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.boughtListTabBar.tintColor = UserStorageManager.getColor()
    }
    

}

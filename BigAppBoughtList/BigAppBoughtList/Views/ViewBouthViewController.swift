//
//  ViewBouthViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 8.09.21.
//

import UIKit

class ViewBouthViewController: UIViewController {

    @IBOutlet weak var priceIntLabel: UILabel!
    @IBOutlet weak var priceBoughtLabel: UILabel!
    @IBOutlet weak var nameCategoryLabel: UILabel!
    @IBOutlet weak var dateBoughtLabel: UILabel!
    @IBOutlet weak var fotoBoughtImageView: UIImageView!
    
    var boughtModel: BoughtModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            fotoBoughtImageView.image = UIImage(named: "purchases")
        
        if let boughtModel: BoughtModel = boughtModel{
            priceIntLabel.text = String(boughtModel.price)
            priceBoughtLabel.text = "Цена:"
            nameCategoryLabel.text = boughtModel.category.categoryName
            let date: Date = Date(timeIntervalSince1970: boughtModel.dateBought)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            self.dateBoughtLabel.text = formatter.string(from: date)
            if let imageData = boughtModel.photoBought, let image = UIImage(data: imageData) {
                self.fotoBoughtImageView.image = image
            }
        }

    }

}

//
//  SettingColorViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 9.09.21.
//

import UIKit

class SettingColorViewController: UIViewController {
    
    @IBOutlet weak var colorYellouButton: UIButton!
    @IBOutlet weak var colorRedButton: UIButton!
    @IBOutlet weak var colorBlueButton: UIButton!
    @IBOutlet weak var colorPurpleButton: UIButton!
    @IBOutlet weak var colorDarkBlueButton: UIButton!
    @IBOutlet weak var colorOrangeButton: UIButton!
    @IBOutlet weak var colorGreenButton: UIButton!
    
    
    @IBOutlet weak var colorLabel: UILabel!
    
    @IBOutlet weak var separatorColorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.colorLabel.text = "Выбрать цвет"

        self.separatorColorView.backgroundColor = UserStorageManager.getColor()
        let imageYellow: UIImage? = UIImage(named: "Yelloy")?.withRenderingMode(.alwaysOriginal)
        self.colorYellouButton.setImage(imageYellow, for: UIControl.State.normal)
        
        let imageRed: UIImage? = UIImage(named: "Red")?.withRenderingMode(.alwaysOriginal)
        self.colorRedButton.setImage(imageRed, for: UIControl.State.normal)
        
        let imageBlue: UIImage? = UIImage(named: "Blue")?.withRenderingMode(.alwaysOriginal)
        self.colorBlueButton.setImage(imageBlue, for: UIControl.State.normal)
               
        let imagePurple: UIImage? = UIImage(named: "Purple")?.withRenderingMode(.alwaysOriginal)
        self.colorPurpleButton.setImage(imagePurple, for: UIControl.State.normal)
         
        let imageDarkBlue: UIImage? = UIImage(named: "DarkBlue")?.withRenderingMode(.alwaysOriginal)
        self.colorDarkBlueButton.setImage(imageDarkBlue, for: UIControl.State.normal)
        
        let imageOrange: UIImage? = UIImage(named: "Orange")?.withRenderingMode(.alwaysOriginal)
        self.colorOrangeButton.setImage(imageOrange, for: UIControl.State.normal)
             
        let imageGreen: UIImage? = UIImage(named: "Green")?.withRenderingMode(.alwaysOriginal)
        self.colorGreenButton.setImage(imageGreen, for: UIControl.State.normal)
                         
    }
    
    @IBAction func colorYellouActionButton(_ sender: Any) {
        UserStorageManager.saveColor(color: .yellowColor)
    }
    
    @IBAction func colorRedActionButton(_ sender: Any) {
        UserStorageManager.saveColor(color: .redColor)
    }
    
    @IBAction func colorBlueActionButton(_ sender: Any) {
        UserStorageManager.saveColor(color: .blueColor)
    }
    
    @IBAction func colorPurpleActionButton(_ sender: Any) {
        UserStorageManager.saveColor(color: .purpleColor)
    }
    
    @IBAction func colorDarkBlueActionButton(_ sender: Any) {
        UserStorageManager.saveColor(color: .darkBlueColor)
    }
    
    @IBAction func colorOrangeActionButton(_ sender: Any) {
        UserStorageManager.saveColor(color: .orangeColor)
    }
    
    @IBAction func colorGreenActionButton(_ sender: Any) {
        UserStorageManager.saveColor(color: .greenColor)
    }
    
}

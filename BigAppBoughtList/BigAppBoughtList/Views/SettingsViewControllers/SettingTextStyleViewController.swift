//
//  SettingTextStyleViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 9.09.21.
//

import UIKit

class SettingTextStyleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var settingTextStyleTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingTextStyleTableView.delegate = self
        self.settingTextStyleTableView.dataSource = self
        
        self.settingTextStyleTableView.register(UINib(nibName: SettingTextStyleTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: SettingTextStyleTableViewCell.reuseId)
        
        self.settingTextStyleTableView.separatorStyle = .none
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SettingTextStyleTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingTextStyleTableViewCell = self.settingTextStyleTableView.dequeueReusableCell(withIdentifier: SettingTextStyleTableViewCell.reuseId, for: indexPath) as! SettingTextStyleTableViewCell
        cell.selectionStyle = .none
        if indexPath.row == 0{
            cell.textStyleLabel.text = "Standart"
            cell.textStyleLabel.font = UIFont.systemFont(ofSize: 18)
        }
        if indexPath.row == 1{
            cell.textStyleLabel.text = "Roboto"
            cell.textStyleLabel.font = UIFont.robotoRegularFont(size: 18)
        }
        if indexPath.row == 2{
            cell.textStyleLabel.text = "Oswald"
            cell.textStyleLabel.font = UIFont.oswaldRegularFont(size: 18)

        }
        if indexPath.row == 3{
            cell.textStyleLabel.text = "Pangolin"
            cell.textStyleLabel.font = UIFont.pangolinRegularFont(size: 18)

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            UserStorageManager.saveFont(font: .system)
        }
        if indexPath.row == 1{
            UserStorageManager.saveFont(font: .roboto)
        }
        if indexPath.row == 2{
            UserStorageManager.saveFont(font: .oswald)
        }
        if indexPath.row == 3{
            UserStorageManager.saveFont(font: .pangolin)
        }
    }
    
}

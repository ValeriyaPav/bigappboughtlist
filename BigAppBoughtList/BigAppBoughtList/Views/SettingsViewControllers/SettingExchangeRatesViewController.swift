//
//  SettingExchangeRatesViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 9.09.21.
//

import UIKit

class SettingExchangeRatesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var settingExchangeRatesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.settingExchangeRatesTableView.delegate = self
        self.settingExchangeRatesTableView.dataSource = self
        
        self.settingExchangeRatesTableView.register(UINib(nibName: SettingExchangeRatesTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: SettingExchangeRatesTableViewCell.reuseId)
        
        self.settingExchangeRatesTableView.separatorStyle = .none
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SettingExchangeRatesTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingExchangeRatesTableViewCell = tableView.dequeueReusableCell(withIdentifier: SettingExchangeRatesTableViewCell.reuseId, for: indexPath) as! SettingExchangeRatesTableViewCell
        cell.selectionStyle = .none
        if indexPath.row == 0{
            cell.exchangeLabel.text = "Доллар:"
        }
        return  cell
    }


}

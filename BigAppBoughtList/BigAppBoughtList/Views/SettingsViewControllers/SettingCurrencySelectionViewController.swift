//
//  SettingCurrencySelectionViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 9.09.21.
//

import UIKit

class SettingCurrencySelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    @IBOutlet weak var settingCurrencySelectionTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.settingCurrencySelectionTableView.delegate = self
        self.settingCurrencySelectionTableView.dataSource = self
        
        self.settingCurrencySelectionTableView.register(UINib(nibName: SettingCurrencySelectionTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: SettingCurrencySelectionTableViewCell.reuseId)
        
        self.settingCurrencySelectionTableView.separatorStyle = .none
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SettingCurrencySelectionTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingCurrencySelectionTableViewCell = self.settingCurrencySelectionTableView.dequeueReusableCell(withIdentifier: SettingCurrencySelectionTableViewCell.reuseId, for: indexPath) as! SettingCurrencySelectionTableViewCell
        cell.selectionStyle = .none
        if indexPath.row == 0{
            cell.nameCurrencyLabel.text = "Белорусский рубль"
        }
        if indexPath.row == 1{
            cell.nameCurrencyLabel.text = "Доллар"
        }
        return cell
    }

    
}

//
//  SettingsViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var settingsTableView: UITableView!
    var baseColor = UserStorageManager.getColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UserStorageManager.getColor()

        
        self.settingsTableView.delegate = self
        self.settingsTableView.dataSource = self
        
        
        self.view.backgroundColor = baseColor
        self.settingsTableView.backgroundColor = baseColor
        
        self.settingsTableView.register(UINib(nibName: SettingsTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: SettingsTableViewCell.reuseId)
        
        self.settingsTableView.register(UINib(nibName: DarkThemeTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: DarkThemeTableViewCell.reuseId)
        
        self.settingsTableView.separatorStyle = .none
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 {
            return DarkThemeTableViewCell.cellHeight
        } else {
            return SettingsTableViewCell.cellHeight
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 || section == 0 {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0
        }
        if section == 1{
            return 15.0
        } else {
            return 25.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: UIView = UIView()
        view.backgroundColor = baseColor
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            self.pushSettingColorViewController()
        }
        if indexPath.section == 0 && indexPath.row == 1 {
            self.pushSettingTextStyleViewController()
        }
        if indexPath.section == 1 && indexPath.row == 0 {
            self.pushCurrencySelectionViewController()
        }
        if indexPath.section == 1 && indexPath.row == 1 {
            self.pushExchangeRatesViewController()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 2 && indexPath.row == 0 {
            let cell: DarkThemeTableViewCell = self.settingsTableView.dequeueReusableCell(withIdentifier: DarkThemeTableViewCell.reuseId, for: indexPath) as! DarkThemeTableViewCell
            cell.selectionStyle = .none
            cell.nameDarkThemeSettingLabel.text = "Темная тема"
            return cell
        } else {
            let cell: SettingsTableViewCell = self.settingsTableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.reuseId, for: indexPath) as! SettingsTableViewCell
            cell.selectionStyle = .none
            if indexPath.section == 0 && indexPath.row == 0{
                cell.nameSettingLabel.text = "Цвет приложения"
            }
            if indexPath.section == 0 && indexPath.row == 1{
                cell.nameSettingLabel.text = "Шрифт"
            }
            if indexPath.section == 1 && indexPath.row == 0{
                cell.nameSettingLabel.text = "Выбор валюты"
            }
            if indexPath.section == 1 && indexPath.row == 1{
                cell.nameSettingLabel.text = "Текущий курс"
            }
            return cell
        }
        
    }
    
    ///
    func pushSettingColorViewController() {
        if let settingColorViewController: SettingColorViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingColorViewController") as? SettingColorViewController {
            self.navigationController?.pushViewController(settingColorViewController, animated: true)
        }
    }
    
    func pushSettingTextStyleViewController() {
        if let settingTextStyleViewController: SettingTextStyleViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingTextStyleViewController") as? SettingTextStyleViewController {
            self.navigationController?.pushViewController(settingTextStyleViewController, animated: true)
        }
    }
    
    func pushCurrencySelectionViewController() {
        if let settingCurrencySelectionViewController: SettingCurrencySelectionViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingCurrencySelectionViewController") as? SettingCurrencySelectionViewController {
            self.navigationController?.pushViewController(settingCurrencySelectionViewController, animated: true)
        }
    }
    
    func pushExchangeRatesViewController() {
        if let settingExchangeRatesViewController: SettingExchangeRatesViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingExchangeRatesViewController") as? SettingExchangeRatesViewController {
            self.navigationController?.pushViewController(settingExchangeRatesViewController, animated: true)
        }
    }
    
    
    
}

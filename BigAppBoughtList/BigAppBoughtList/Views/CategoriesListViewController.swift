//
//  CategoriesListViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit


class CategoriesListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    
    private let countCellsInRow = 2
    private let offset: CGFloat = 2.0

    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UserStorageManager.getColor()

        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        
        
        categoryCollectionView.register(UINib(nibName: CategoriesCollectionViewCell.reuseId, bundle: nil), forCellWithReuseIdentifier: CategoriesCollectionViewCell.reuseId)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoriesCollectionViewCell.reuseId, for: indexPath) as! CategoriesCollectionViewCell
        let category: Category = Category(rawValue: indexPath.row)!
        cell.nameCategoriesLabel.text = category.categoryName
        cell.categoriesImageView.image = UIImage(named: category.categoryImageName)!
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath ) -> CGSize{
        
        let frameCV = collectionView.frame
        let widthCell = frameCV.width / CGFloat(countCellsInRow) - 20
        let hightCell = CGFloat(widthCell - 30)
        
        let spacing = CGFloat((countCellsInRow + 1)) * offset / CGFloat(countCellsInRow)
        
        return CGSize(width: widthCell - spacing, height: hightCell - (offset * 2))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let transactionsListViewController: TransactionsListViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsListViewController") as? TransactionsListViewController {
            transactionsListViewController.state = .byCategory
            let category: Category = Category(rawValue: indexPath.row)!
            transactionsListViewController.selectCategory = category.categoryName
            self.navigationController?.pushViewController(transactionsListViewController, animated: true)  
        }
    }
    

}

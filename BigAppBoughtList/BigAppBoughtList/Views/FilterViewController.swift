//
//  FilterViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 8.09.21.
//

import UIKit

protocol FilterViewControllerDelegate: AnyObject {
    func filterBoughtModelsArray(category: Category?)
}


class FilterViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var categoryFilterPickerView: UIPickerView!
    @IBOutlet weak var nameCategoryFilterLabel: UILabel!
    @IBOutlet weak var filterDateTextField: UITextField!
    @IBOutlet weak var nameFilterDateLabel: UILabel!
    
    var datePicker = UIDatePicker()
    weak var delegate: FilterViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.preferredDatePickerStyle = .wheels
        
        createDatePicker()
        
        self.view.backgroundColor = UserStorageManager.getColor()
        self.categoryFilterPickerView.delegate = self
        self.categoryFilterPickerView.dataSource = self
        
        self.nameCategoryFilterLabel.text = "Категория"
        self.nameCategoryFilterLabel.font = UserStorageManager.getFont(size: 17)
        self.nameFilterDateLabel.text = "Дата покупки"
        self.nameFilterDateLabel.font = UserStorageManager.getFont(size: 15)
        self.filterButton.layer.cornerRadius = 18.0
        self.filterButton.backgroundColor = UIColor.myButtonColor
        self.filterButton.setTitle("Применить", for: .normal)
        self.filterButton.titleLabel?.font = UserStorageManager.getFont(size: 17)
    }
    
    @IBAction func addFilterActionButton(_ sender: Any) {
        let pickerIndex: Int = categoryFilterPickerView.selectedRow(inComponent: 0)
        if pickerIndex <= 0 {
            self.delegate?.filterBoughtModelsArray(category: nil)
        } else {
            if let selectFilter: Category = Category(rawValue: pickerIndex - 1) {
                self.delegate?.filterBoughtModelsArray(category: selectFilter)
            }
        }
        self.dismissFilterViewController()
    }
    
    func dismissFilterViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Category.allCases.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var nameCategory: String?
        if row == 0 {
            nameCategory = "Без фильтра"
        } else {
            if let categoryFilter: Category = Category(rawValue: row - 1) {
                switch categoryFilter {
                case .food:
                    nameCategory = "Продукты"
                case .restaurants:
                    nameCategory = "Рестораны"
                case .payments:
                    nameCategory = "Платежи"
                case .travel:
                    nameCategory = "Проезд"
                case .plays:
                    nameCategory = "Игры"
                case .entertainment:
                    nameCategory = "Развлечения"
                case .clothes:
                    nameCategory = "Одежда"
                case .householdChemicals:
                    nameCategory = "Быт - Хим"
                case .appliances:
                    nameCategory = "Техника"
                case .medicine:
                    nameCategory = "Медицина"
                case .other:
                    nameCategory = "Прочее"
                }
            }
        }
        return nameCategory
    }
    
    func createDatePicker(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        
        toolbar.setItems([doneButton], animated: true)
        
        filterDateTextField.inputAccessoryView = toolbar
        
        filterDateTextField.inputView = datePicker
        
        datePicker.datePickerMode = .date
        
    }
    
    @objc func donePressed(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        filterDateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }

}

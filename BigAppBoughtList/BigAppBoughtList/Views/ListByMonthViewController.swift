//
//  ViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit

class ListByMonthViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var transactionsArray: [BoughtModel] = UserStorageManager.getBoughtArray()
        
    var monthesArray: [Date] {
        transactionsArray.sort {
            return $0.dateBought < $1.dateBought
        }
        if let firstElement: BoughtModel = transactionsArray.first, let lastElement: BoughtModel = transactionsArray.last {
            let firstTransactionStartOfMonth: Date = Date(timeIntervalSince1970: firstElement.dateBought).startOfMonth
            let lastTransactionStartOfMonth: Date = Date(timeIntervalSince1970: lastElement.dateBought).startOfMonth
            let monthes: [Date] = Date.differenceInMonthesBetweenTwoDates(firstDate: firstTransactionStartOfMonth, lastDate: lastTransactionStartOfMonth)
            return monthes.reversed()
        }
        return []
    }
    
    @IBOutlet weak var monthTableView: UITableView!
    var baseColor = UserStorageManager.getColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UserStorageManager.getColor()

        
        self.monthTableView.delegate = self
        self.monthTableView.dataSource = self
        
        self.monthTableView.register(UINib(nibName: MonthListTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: MonthListTableViewCell.reuseId)
        
        self.monthTableView.separatorStyle = .none
        self.navigationController?.navigationBar.backgroundColor = baseColor
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        transactionsArray = UserStorageManager.getBoughtArray()
        self.monthTableView.reloadData()
 
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MonthListTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return monthesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MonthListTableViewCell = self.monthTableView.dequeueReusableCell(withIdentifier: MonthListTableViewCell.reuseId, for: indexPath) as! MonthListTableViewCell
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM yyyy"
        cell.nameMonthLabel.text = formatter.string(from: monthesArray[indexPath.row]).capitalized
        cell.selectionStyle = .none
        cell.allSpentSumLabel.text = String(calculateSumInMonth(array: filterBoughtOnMonth(array: transactionsArray, startDate: monthesArray[indexPath.row], endDate: monthesArray[indexPath.row].endOfMonth)))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let transactionsListViewController: TransactionsListViewController = self.storyboard?.instantiateViewController(withIdentifier: "TransactionsListViewController") as? TransactionsListViewController {
            transactionsListViewController.state = .byMonth
            transactionsListViewController.firstDayMonth = monthesArray[indexPath.row]
            transactionsListViewController.lastDayMonth = monthesArray[indexPath.row].endOfMonth
            self.navigationController?.pushViewController(transactionsListViewController, animated: true)
        }
    }
    
    func filterBoughtOnMonth(array: [BoughtModel], startDate: Date, endDate: Date) -> [BoughtModel]{
        let filterDateArray = array.filter { Date(timeIntervalSince1970: $0.dateBought) >= startDate && Date(timeIntervalSince1970: $0.dateBought) <= endDate }
        return filterDateArray
    }
    
    func calculateSumInMonth(array: [BoughtModel]) -> Double {
        let sumInMonth = array.reduce (0.0,{ res, element in
            res + element.price
        })
        return sumInMonth
    }

}

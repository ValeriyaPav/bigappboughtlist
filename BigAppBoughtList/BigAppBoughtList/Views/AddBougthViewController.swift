//
//  AddBouthViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 8.09.21.
//

import UIKit

protocol AddBoughtViewControllerDelegate: AnyObject {
    func appendBoughtModel(model: BoughtModel)
}

class AddBougthViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addFotoButton: UIButton!
    @IBOutlet weak var addBoughtButton: UIButton!
    @IBOutlet weak var categoryPickerView: UIPickerView!
    @IBOutlet weak var nameCategoryLabel: UILabel!
    @IBOutlet weak var dateBoughtTextField: UITextField!
    @IBOutlet weak var dateBoughtLabel: UILabel!
    @IBOutlet weak var priceBoughtTextField: UITextField!
    @IBOutlet weak var priceBoughtLabel: UILabel!
    @IBOutlet weak var boughtImageView: UIImageView!
    
    private var image: UIImage?
    private let uiImagePickerController = UIImagePickerController()
    
    var datePicker = UIDatePicker()
    
    weak var delegate: AddBoughtViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.preferredDatePickerStyle = .wheels
        
        createDatePicker()
        
        self.descriptionLabel.text = "Описание покупки"
        self.descriptionTextField.delegate = self
        self.dateBoughtTextField.delegate = self
        self.categoryPickerView.delegate = self
        self.categoryPickerView.dataSource = self
        
        self.nameCategoryLabel.text = "Категория"
        self.dateBoughtLabel.text = "Дата покупки"
        self.priceBoughtLabel.text = "Цена покупки"
        
        self.addBoughtButton.layer.cornerRadius = 18.0
        self.addBoughtButton.backgroundColor = .myButtonColor
        self.addBoughtButton.setTitle("Добавить", for: .normal)
        
        self.boughtImageView.layer.cornerRadius = 18.0
        self.boughtImageView.layer.borderWidth = 1.0
        self.boughtImageView.layer.borderColor = UserStorageManager.getColor().cgColor
        self.addFotoButton.setImage(UIImage(named: "addFoto"), for: UIControl.State.normal)
        self.addFotoButton.tintColor = UserStorageManager.getColor()
        
    }
    
    @IBAction func addFotoActionButton(_ sender: Any) {
        uiImagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
        uiImagePickerController.delegate = self
        self.present(uiImagePickerController, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.boughtImageView.image = image
            self.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addBoughtActionButton(_ sender: Any) {
        if isAllTrue(price: errorPrice(), date: errorDate()){
            if let priceString = priceBoughtTextField.text{
                if let categoryPickerView = categoryPickerView, let type: Category = Category(rawValue: categoryPickerView.selectedRow(inComponent: 0)) {
                    let model: BoughtModel = BoughtModel(id: UUID().uuidString, price: Double(priceString) ?? 0.0 , categoryImageName: type.categoryImageName, category: type, dateBought: datePicker.date.timeIntervalSince1970, description: descriptionTextField.text ?? "", photoBought: image?.pngData())
                    self.delegate?.appendBoughtModel(model: model)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Category.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var nameCategory: String?
        if let category: Category = Category(rawValue: row) {
            switch category {
            case .food:
                nameCategory = "Продукты"
            case .restaurants:
                nameCategory = "Рестораны"
            case .payments:
                nameCategory = "Платежи"
            case .travel:
                nameCategory = "Проезд"
            case .plays:
                nameCategory = "Игры"
            case .entertainment:
                nameCategory = "Развлечения"
            case .clothes:
                nameCategory = "Одежда"
            case .householdChemicals:
                nameCategory = "Быт - Хим"
            case .appliances:
                nameCategory = "Техника"
            case .medicine:
                nameCategory = "Медицина"
            case .other:
                nameCategory = "Прочее"
            }
        }
        return nameCategory
    }
    
    func isAllTrue(price: Bool, date: Bool) -> Bool{
        if price == false && date == false{
            return true
        }else{
            return false
        }
    }
    
    func errorPrice() -> Bool {
        if priceBoughtTextField.text == ""{
            self.priceBoughtLabel.text = "Введите цену покупки"
            return true
        }
        self.priceBoughtLabel.text = "Цена покупки"
        return false
    }
    
    func errorDate() -> Bool {
        if dateBoughtTextField.text == ""{
            self.dateBoughtLabel.text = "Выберите дату покупки"
            return true
        }
        self.dateBoughtLabel.text = "Дата покупки"
        return false
    }
    
    
    func createDatePicker(){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        
        toolbar.setItems([doneButton], animated: true)
        
        dateBoughtTextField.inputAccessoryView = toolbar
        
        dateBoughtTextField.inputView = datePicker
        
        datePicker.datePickerMode = .date
        
    }
    
    @objc func donePressed(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        dateBoughtTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
}

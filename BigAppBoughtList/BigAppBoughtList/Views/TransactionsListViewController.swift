//
//  TransactionsListViewController.swift
//  BigAppBoughtList
//
//  Created by Валерия П on 7.09.21.
//

import UIKit


enum StateTransaction: Int, CaseIterable, Codable {
    case base
    case byCategory
    case byMonth
}

class TransactionsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AddBoughtViewControllerDelegate, FilterViewControllerDelegate {
    
    
    
    @IBOutlet var filterButton: UIBarButtonItem!
    @IBOutlet var addButton: UIBarButtonItem!
    @IBOutlet var transactionTabelView: UITableView!
    
    var state: StateTransaction = .base
    
    var firstDayMonth: Date?
    var lastDayMonth: Date?
    var selectCategory: String?
    
    
    private var boughtsArray: [BoughtModel] = UserStorageManager.getBoughtArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UserStorageManager.getColor()
        
        switch state {
        case .base:
            filterMonth()
            self.addButton = UIBarButtonItem(title: "Add", image: UIImage(named: "add"), primaryAction: UIAction(handler: { _ in
                self.pushAddViewController()
            }), menu: nil)
            self.navigationItem.rightBarButtonItem = addButton
            self.navigationItem.rightBarButtonItem?.tintColor = UserStorageManager.getColor()

            
            self.filterButton = UIBarButtonItem(title: "Filter", image: UIImage(named: "filter"), primaryAction: UIAction(handler: { _ in
                self.presentFilterViewController()
            }), menu: nil)
            self.navigationItem.leftBarButtonItem = filterButton
            self.navigationItem.leftBarButtonItem?.tintColor = UserStorageManager.getColor()
        case .byCategory:
            if let selectCategory: String = selectCategory{
                filterByCategory(selectCategory: selectCategory)
            }
            self.navigationItem.rightBarButtonItem = nil
            self.filterButton = UIBarButtonItem(title: nil, image: UIImage(named: "back"), primaryAction: UIAction(handler: { _ in
                self.navigationController?.popViewController(animated: true)
            }), menu: nil)
            self.navigationItem.leftBarButtonItem = filterButton
            self.navigationItem.leftBarButtonItem?.tintColor = UserStorageManager.getColor()
        case .byMonth:
            if let firstDayMonth: Date = firstDayMonth, let lastDayMonth: Date = lastDayMonth{
                boughtsArray = filterBoughtOnMonth(array: boughtsArray, startDate: firstDayMonth, endDate: lastDayMonth)
            }
            self.navigationItem.rightBarButtonItem = nil
            self.filterButton = UIBarButtonItem(title: nil, image: UIImage(named: "back"), primaryAction: UIAction(handler: { _ in
                self.navigationController?.popViewController(animated: true)
            }), menu: nil)
            self.navigationItem.leftBarButtonItem = filterButton
            self.navigationItem.leftBarButtonItem?.tintColor = UserStorageManager.getColor()
        }
        
        self.transactionTabelView.delegate = self
        self.transactionTabelView.dataSource = self
        self.transactionTabelView.separatorStyle = .none
        
        self.transactionTabelView.register(UINib(nibName: TransactionTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: TransactionTableViewCell.reuseId)
        
        self.navigationController?.navigationBar.backgroundColor = UserStorageManager.getColor()
        
        
    }
    
    func pushAddViewController() {
        if let addBougthViewController: AddBougthViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddBougthViewController") as? AddBougthViewController {
            addBougthViewController.delegate = self
            self.navigationController?.pushViewController(addBougthViewController, animated: true)
        }
    }
    
    func presentFilterViewController() {
        if let filterViewController: FilterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
            filterViewController.delegate = self
            self.present(filterViewController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TransactionTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boughtsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        UserStorageManager.removeBoughtModel(id: boughtsArray[indexPath.row].id)
        boughtsArray.remove(at: indexPath.row)
        self.transactionTabelView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TransactionTableViewCell = self.transactionTabelView.dequeueReusableCell(withIdentifier: TransactionTableViewCell.reuseId, for: indexPath) as! TransactionTableViewCell
        cell.configureWithModel(model: boughtsArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewBouthViewController: ViewBouthViewController =
            self.storyboard?.instantiateViewController(withIdentifier: "ViewBouthViewController") as? ViewBouthViewController {
            viewBouthViewController.boughtModel = self.boughtsArray[indexPath.row]
            self.navigationController?.pushViewController(viewBouthViewController, animated: true)        }
    }
    
    func filterBoughtModelsArray(category: Category?) {
        filterMonth()
        if let category: Category = category {
            var filteredArray: [BoughtModel] = []
            for element in boughtsArray {
                if element.category == category {
                    filteredArray.append(element)
                }
            }
            self.boughtsArray = filteredArray
            self.transactionTabelView.reloadData()
        }
    }
    
    func filterByCategory(selectCategory: String) {
        var filteredArray: [BoughtModel] = []
        for element in boughtsArray {
            if element.category.categoryName == selectCategory {
                filteredArray.append(element)
            }
        }
        self.boughtsArray = filteredArray
    }
    
    
    
    
    func appendBoughtModel(model: BoughtModel) {
        UserStorageManager.addBoughtModel(model: model)
        boughtsArray.append(model)
        filterMonth()
        self.transactionTabelView.reloadData()
    }
    
    func filterMonth() {
        let dateToday: Date = Date()
        let firstDayOfMonth: Date = dateToday.startOfMonth
        let filterDateArray = UserStorageManager.getBoughtArray().filter { Date(timeIntervalSince1970: $0.dateBought) >= firstDayOfMonth }
        self.boughtsArray = filterDateArray
        self.transactionTabelView.reloadData()
    }
    
    func filterBoughtOnMonth(array: [BoughtModel], startDate: Date, endDate: Date) -> [BoughtModel]{
        let filterDateArray = array.filter { Date(timeIntervalSince1970: $0.dateBought) >= startDate && Date(timeIntervalSince1970: $0.dateBought) <= endDate }
        return filterDateArray
    }
    
    
}
